/**
 * represents a mathematical permutation
*/
var permutation = function(pairs){
	return {
		/**
		 * @cfg {Array} pairs
		 *
		 * Should not be accessed directly
		*/
		pairs: pairs,
		
		/**
		 * test method to check if the class was loaded properly
		*/
		ping: function(){
			alert('test');
		},


		/**
		 * inverses the permutation and retrieves a new one
		 * 
		 * @returns {permutation} inverted permutation
		*/
		invert: function(){
			// first we need to invert all pairs in a temporary array
			var tempPairs = this.getPairs();

			tempPairs.forEach(function(pair){
				var firstPairTmp = pair[0];

				pair[0] = pair[1];
				pair[1] = firstPairTmp;
			});

			// return new permutation with the flipped values
			return new permutation(tempPairs);
		},

		/**
		 * retrieves the first pair of this permutation with the given argument
		*/
		getPairByArgument: function(argument){
			var pairs = this.getPairs();

			for (var i = 0; i < pairs.length; i++){
				var pair = pairs[i];

				if (pair[0] === argument) return pair;
			}

			return null;
		},

		/**
		 * composites the current permutation with the specified one
		 * and retrieves a new one
		 *
		 * @returns {permutation} result
		*/
		compose: function(other){
			var self = this;
			var newPairs = [];

			other.getPairs().forEach(function(otherPair){
				console.log(self.getPairByArgument(otherPair[1]));

				startValue = otherPair[0];
				endValue = self.getPairByArgument(otherPair[1])[1];

				newPairs.push([startValue, endValue]);
			});

			return new permutation(newPairs);
		},

		/**
		 * returns the current permutation as string representation
		*/
		toString: function(){
			var representation = "[   ";

			this.getPairs().forEach(function(pair){
				representation += pair[0]+","+pair[1] + "    ";
			});

			representation += "]";

			return representation;
		},

		/**
 		 * returns the current permutation as cyclinc string representation
		*/
		toStringCyclic: function(){
			var representation = "",
				me = this,
				pairs = this.getPairs(),
				usedPairs = [];

			pairs.forEach(function(pair){
				if (usedPairs.indexOf(pair) === -1){
					// not found in the used stack, process element

					if (pair[0] === pair[1]){
						// when the pair has the same value
						// it's a single element
						representation += "("+pair[0]+")";
					} else {
						// start searching for elements to chain
						representation += "(";
						representation += pair[0]+","

						var startValue = pair[0],
							currentEndPoint = pair[1],
							currentPair = pair;
						
						// subloop to chain all linked permutation pairs
						while (
							// the current end point must be not the target of the current pair
							// this cannot be the case, but we have it here for additional security
							currentEndPoint !== currentPair[0] 
							// the current pairs target value cannot must not be the same as the argument
							// of the initial pair from which the search was initialized
							&& startValue !== currentPair[1]
						){
							// fetch next pair according to argument
							var tempPair = me.getNextChainedPair(pairs, usedPairs, currentEndPoint);

							// sometimes no target element is found for the current pass
							// so we check for null
							if (tempPair !== null){
								// add to representation
								representation += tempPair[0]+",";
								// and push it into the used pairs stack
								usedPairs.push(tempPair);

								// replace the value of the currentpair
								currentPair = tempPair;
								// select the next end point
								currentEndPoint = currentPair[1];
							}
						}

						// ... remove last comma added which is basicallly just the last char of the string
						representation = representation.substring(0, representation.length - 1);

						// close with delimiter and we're done for this particular field
						representation += ")";
					}

					// after processing, add to used list
					usedPairs.push(pair);
				}
			});

			return representation;
		},

		/**
		 * get next pair which was chained to the current one specified
		*/
		getNextChainedPair: function(pairs, usedPairs, argument){
			for (var i = 0; i < pairs.length; i++){
				var currentPair = pairs[i];

				if (usedPairs.indexOf(currentPair) === -1 && currentPair[0] === argument){
					return currentPair;
				}
			}

			return null;
		},
		
		/**
		 * get the pairs for this permutation in raw format
		 * 
		 * @returns {Array} pairs
		*/
		getPairs: function(){
			return this.pairs;
		},

		/**
		 * set the pairs
		 * 
		 * @param {Array} pairs
		*/
		setPairs: function(pairs){
			this.pairs = pairs;
		}
	};
};
